package main

import (
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

// CreateCon in charge of connecting mysql
func CreateCon() *gorm.DB {

	// The db connection variables are defined in the OpenShift secrets
	db, err := gorm.Open(os.Getenv("DB_TYPE"), os.Getenv("DB_CONNECTION"))
	db.AutoMigrate(&Redirection{})

	if err != nil {
		fmt.Errorf("MySQL db is not connected, error: %v", err.Error())
	} else {
		fmt.Printf("DB connected")
	}
	return db
}
