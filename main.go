package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"k8s.io/klog"
)

const (
	// TODO: refer const in string
	httpPort = "8080"
)

// Create connection to the DB
var con = CreateCon()

func main() {

	// We need this to initialize global flags
	klog.InitFlags(nil)

	// Create route for redirect http request
	//redirectController()

	router = chi.NewRouter()

	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*", "http://test-webservices-frontend.web.cern.ch"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	router.Use(cors.Handler)

	router.Use(middleware.Recoverer)

	// Start server in port 8080
	klog.Infof("Starting server on port 8080")
	if err := http.ListenAndServe(":8080", redirectController()); err != nil {
		klog.Fatal(err)
	}
}
