package main

import (
	"net/http"
	"net/url"
	"strings"

	"k8s.io/klog"
)

// Manages the URL redirection
func redirectService(w http.ResponseWriter, r *http.Request) {
	var alias = getAlias(r.URL)
	var newURL = findAliasInDb(alias) + "/" + getPath(r.URL)
	klog.Infof("redirect: " + newURL)

	http.Redirect(w, r, newURL, http.StatusSeeOther)
}

// Split the path we receive in three.
// With this we separate the alias from the rest of the URL.
func splitPath(url *url.URL) []string {
	return strings.SplitN(url.Path, "/", 3)
}

// Gets the alias from the path which is in index one (first position is empty as first character of the path is '/').
func getAlias(url *url.URL) string {
	return splitPath(url)[1]
}

// Return path without alias
func getPath(url *url.URL) string {
	var path = splitPath(url)
	if len(path) == 3 {
		return splitPath(url)[2]
	}
	return ""
}

// Get alias URL if any in the DB
func findAliasInDb(alias string) string {
	var redirection Redirection
	con.First(&redirection, "alias = ?", alias)

	return redirection.Redirecturl
}
