package main

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"

	_ "github.com/mattn/go-sqlite3"
)

var router *chi.Mux

// Creates structure of the DB
type Redirection struct {
	gorm.Model         // adds timestamp created_at, updated_at, deleted_at
	Alias       string `gorm:"unique;not null"`
	Redirecturl string `gorm:"not null"`
	Owner       string `gorm:"not null"`
}

// Create route for redirect http request
func redirectController() *chi.Mux {
	//	http.HandleFunc("/", redirectService)
	return routers()
}

func routers() *chi.Mux {
	router.Get("/api/redirections", GetAllRedirections)
	router.Post("/api/redirections", CreateRedirection)
	router.Delete("/api/redirections/{id}", DeleteRedirection)
	// TODO: add updateRedirection method
	//router.Put("/api/redirections/{id}", UpdateRedirection)

	return router
}

// GetAllRedirections create a new post
func GetAllRedirections(w http.ResponseWriter, r *http.Request) {

	var redirections []Redirection
	con.Find(&redirections)
	response, _ := json.Marshal(redirections)

	w.Write(response)
}

// CreateRedirection create a new post
func CreateRedirection(w http.ResponseWriter, r *http.Request) {
	var redirection Redirection
	json.NewDecoder(r.Body).Decode(&redirection)
	redirectionToSave := &Redirection{Alias: redirection.Alias, Redirecturl: redirection.Redirecturl, Owner: "isantosp"}

	// TODO: change owner to point to the creator of the redirect URL
	result := con.Create(redirectionToSave)
	var response []byte
	if result.Error == nil {
		response, _ = json.Marshal(redirectionToSave)
		w.Write(response)
	} else {
		response, _ = json.Marshal(result.Error)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(response)
	}

}

// DeleteRedirection remove a specific post
func DeleteRedirection(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	// TODO: check if the deletion is triggered by the owner of the URL redirection
	con.Where("ID = ?", id).Delete(&Redirection{})
	// TODO: Check if the query returns an error
	w.Write([]byte("Redirection deleted successfully"))
}

// UpdateRedirection update a  spesific post
// func UpdateRedirection(w http.ResponseWriter, r *http.Request) {
// 	var redirection Redirection
// 	json.NewDecoder(r.Body).Decode(&redirection)

// 	query := con.Update(&Redirection{Alias: redirection.Alias, Redirecturl: redirection.Redirecturl, Owner: "isantosp"})
// 	fmt.Println(query.Error)
// 	fmt.Println("Return element", query)
// 	//defer query.Close()

// 	w.Write([]byte("Redirection updated successfully"))

// }
